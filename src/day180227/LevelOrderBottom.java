package day180227;

import java.util.ArrayList;
import java.util.List;

/**
 * author: 李铎
 * date: 2018/2/27
 **/
public class LevelOrderBottom {

    public List<List<Integer>> levelOrderBottom(TreeNode root){
        List<List<Integer>> results = new ArrayList<>();
        if (root == null)
            return results;
        addLevel(results, root, 0);
        return results;
    }

    private void addLevel(List<List<Integer>> list, TreeNode root, int level){
        if (root == null) return;
        if (list.size() - 1 < level)
            list.add(0, new ArrayList<>());
        list.get(list.size() - 1 - level).add(root.val);
        addLevel(list, root.left, level + 1);
        addLevel(list, root.right, level + 1);
    }

}
