package day180227;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * author: 李铎
 * date: 2018/2/27
 **/
public class PascalsTriangle {
    public static List<List<Integer>> generate(int numRows){
        ArrayList<List<Integer>> results = new ArrayList<>();
        ArrayList<Integer> row = new ArrayList<>();
        for (int i = 0; i < numRows; i ++){
            row.add(0, 1);
            for (int j = 1; j < row.size() - 1; j ++){
                row.set(j, row.get(j) + row.get(j + 1));
            }
            results.add(new ArrayList<>(row));
        }
        return results;
    }

    public static void main(String[] args) {
        List<List<Integer>> lists = generate(5);
        for(List<Integer> list : lists) {
            System.out.println(Arrays.toString(list.toArray()));
        }
    }
}
