package day180227;

/**
 * author: 李铎
 * date: 2018/2/1
 **/
public class TreeNode {
    int val;
    TreeNode left;
    TreeNode right;
    TreeNode(int x) { val = x; }
}
