package day190707;

/**
 * @author LI DUO
 * @version 1.0
 * @date 2019/7/7 下午 12:55
 */
public class MaxDepthAfterSplit {
    public int[] maxDepthAfterSplit(String seq) {
        int[] answer = new int[seq.length()];
        int A = 0, B = 0;
        char[] toCharArray = seq.toCharArray();
        for (int i = 0; i < toCharArray.length; i++) {
            if (toCharArray[i] == '(') {
                if (A <= B) {
                    A ++;
                } else {
                    B ++;
                    answer[i] = 1;
                }
            } else {
                if (A > B) {
                    A --;
                } else {
                    B --;
                    answer[i] = 1;
                }
            }
        }
        return answer;
    }
}
