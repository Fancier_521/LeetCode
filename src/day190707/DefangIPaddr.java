package day190707;

/**
 * @author LI DUO
 * @version 1.0
 * @date 2019/7/7 上午 10:31
 */
public class DefangIPaddr {
    public String defangIPaddr(String address) {
        return address.replaceAll("\\.", "[.]");
    }
}
