package day190714;

/**
 * @author LI DUO
 * @version 1.0
 * @date 2019/7/14 上午 10:56
 */
public class LcaDeepestLeaves {

    private TreeNode deepestNode;

    public TreeNode lcaDeepestLeaves(TreeNode root) {
        if (root == null) {
            return null;
        }
        int left = height(root.left);
        int right = height(root.right);
        if (left > right) {
            return lcaDeepestLeaves(root.left);
        }
        if (left < right) {
            return lcaDeepestLeaves(root.right);
        }
        return root;
    }

    private int height(TreeNode p) {
        if (p == null) {
            return 0;
        }
        return 1 + Math.max(height(p.left), height(p.right));
    }



    public class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode(int x) {
            val = x;
        }
    }
}
