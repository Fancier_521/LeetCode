package day190714;

import java.util.Arrays;

/**
 * @author LI DUO
 * @version 1.0
 * @date 2019/7/14 上午 10:33
 */
public class RelativeSortArray {
    public int[] relativeSortArray(int[] arr1, int[] arr2) {
        int[] result = new int[arr1.length];
        Arrays.sort(arr1);
        int k = 0;
        for (int i = 0; i < arr2.length; i++) {
            for (int j = 0; j < arr1.length; j++) {
                if ((arr1[j] != -1) && (arr1[j] == arr2[i])) {
                    result[k ++] = arr1[j];
                    arr1[j] = -1;
                }
            }
        }
        for (int i = 0; i < arr1.length; i++) {
            if (arr1[i] != -1) {
                result[k] = arr1[i];
                k ++;
            }
        }
        return result;
    }
}
