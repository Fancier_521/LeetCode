package day190422;

/**
 * @Author: LI DUO
 * @Date: 2019/4/22 下午 10:50
 * @Version 1.0
 */
public class TitleToNumber {
    public int titleToNumber(String s) {
        if (s == null) {
            return 0;
        }
        int result = 0;
        char[] chars = s.toCharArray();
        for (int i = 0; i < chars.length; i++) {
            char c = chars[i];
            int thisValue = c - 'A' + 1;
            result = result * 26 + thisValue;
        }
        return result;
    }

    public int titleToNumber2(String s) {
        if (s == null) {
            return 0;
        }
        int result = 0;
        int length = s.length();
        for (int i = 0; i < length; i++) {
            int num = s.charAt(length - 1 - i) - 64;
            int j = i;
            while (j > 0) {
                num *= 26;
                j--;
            }
            result += num;
        }
        return result;
    }
}
