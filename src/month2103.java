import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

/**
 * @author Li Duo
 * @version 1.0
 * @date 2021/3/12 15:28
 */
public class month2103 {

    public boolean isValidSerialization(String preorder) {
        int markNum = 0;
        for (int i = preorder.length() - 1; i >= 0; i--) {
            char at = preorder.charAt(i);
            if (at == '#') {
                markNum++;
            } else if (at == ',') {
                continue;
            } else {
                while (i >= 0 && Character.isDigit(preorder.charAt(i))) {
                    i--;
                }
                if (markNum >= 2) {
                    markNum--;
                } else {
                    return false;
                }
            }
        }
        return markNum == 1;
    }

    public int lengthOfLongestSubstring(String s) {
        int n = s.length();
        int[] index = new int[128];
        int ans = 0, i = 0, j = 0;
        for (; j < n; j++) {
            i = Math.max(i, index[s.charAt(j)]);
            ans = Math.max(ans, j - i + 1);
            index[s.charAt(j)] = j + 1;
        }
        return ans;
    }

    private int max = 0;

    public int[] findFrequentTreeSum(TreeNode root) {
        HashMap<Integer, Integer> hashMap = new HashMap<>();
        readTree(root, hashMap);
        List<Integer> res = new LinkedList<>();
        for (Integer i : hashMap.keySet()) {
            if (hashMap.get(i) == max) {
                res.add(i);
            }
        }
        int[] resArr = new int[res.size()];
        for (int i = 0; i < res.size(); i++) {
            resArr[i] = res.get(i);
        }
        return resArr;
    }

    private int readTree(TreeNode root, HashMap<Integer, Integer> hashMap) {
        if (root == null) {
            return 0;
        }
        int left = readTree(root.left, hashMap);
        int right = readTree(root.right, hashMap);
        int value = left + right + root.val;
        hashMap.put(value, hashMap.getOrDefault(value, 1));
        max = Math.max(max, hashMap.get(value));
        return value;
    }

    public static void main(String[] args) {
        month2103 month2103 = new month2103();
        month2103.lengthOfLongestSubstring("abcabcbb");
    }
}
