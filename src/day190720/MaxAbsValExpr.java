package day190720;

/**
 * @author LI DUO
 * @version 1.0
 * @date 2019/7/21 下午 01:39
 */
public class MaxAbsValExpr {
    public int maxAbsValExpr(int[] arr1, int[] arr2) {
        int n = arr1.length;
        int[] a = new int[n + 1], b = new int[n + 1], sum = new int[n + 1];
        for (int i = 0; i < n; ++i) {
            a[i + 1] = arr1[i];
            b[i + 1] = arr2[i];
        }

        int result = 0;
        // i > j
        /*
        a[i]-a[j]+b[i]-b[j]+i-j == (a[i]+b[i]+i) - (a[j]+b[j]+j);
        a[i]-a[j]-b[i]+b[j]+i-j == (a[i]-b[i]+i) - (a[j]-b[j]+j);
        -a[i]+a[j]+b[i]-b[j]+i-j == (-a[i]+b[i]+i) - (-a[j]+b[j]+j);
        -a[i]+a[j]-b[i]+b[j]+i-j == (-a[i]-b[i]+i) - (-a[j]-b[j]-j);
        */

        for (int ka = -1; ka <= 1; ka += 2) {
            for (int kb = -1; kb <= 1; kb += 2) {
                int minv = Integer.MAX_VALUE;
                for (int i = 1; i <= n; ++i) {
                    sum[i] = ka * a[i] + kb * b[i] + i;
                    minv = Math.min(minv, sum[i]);
                    result = Math.max(result, sum[i] - minv);
                }
            }
        }
        return result;
    }
}
