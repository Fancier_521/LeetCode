package day190720;

/**
 * @author LI DUO
 * @version 1.0
 * @date 2019/7/21 下午 01:53
 */
public class NumEquivDominoPairs {
    public int numEquivDominoPairs(int[][] dominoes) {
        int result = 0;
        int[][] sum = new int[10][10];
        for (int i = 0; i < dominoes.length; i++) {
            if (dominoes[i][0] <= dominoes[i][1]) {
                sum[dominoes[i][0]][dominoes[i][1]] ++;
            } else {
                sum[dominoes[i][1]][dominoes[i][0]] ++;
            }
        }
        for (int i = 0; i < sum.length; i++) {
            for (int j = 0; j < sum.length; j++) {
                if (sum[i][j] == 0) {
                    continue;
                }
                result += function(sum[i][j]);
            }
        }
        return result;
    }

    private int function(int n) {
        int result = 0;
        while (n-- > 0) {
            result += n;
        }
        return result;
    }
}
