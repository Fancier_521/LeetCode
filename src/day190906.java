/**
 * @author LI DUO
 * @version 1.0
 * @date 2019/9/6 上午 11:22
 */
public class day190906 {

    public int numIslands(char[][] grid) {
        int island = 0;
        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[i].length; j++) {
                //是陆地
                if (grid[i][j] == '1') {
                    marked(grid, i ,j);
                    island ++;
                }
            }
        }
        return island;
    }

    private void marked(char[][] grid, int i, int j) {
        if (i < 0 || i >= grid.length || j < 0 || j >= grid[0].length || grid[i][j] != '1') {
            return;
        }
        grid[i][j] = '2';
        //分别往 上 下 左 右 递归
        marked(grid, i + 1, j);
        marked(grid, i - 1, j);
        marked(grid, i, j - 1);
        marked(grid, i, j + 1);
    }

    public boolean isValidSudoku(char[][] board) {
        for (int i = 0; i < board.length; i++) {
            int[] numMapCol = new int[9];
            int[] numMapRow = new int[9];
            for (int j = 0; j < board[i].length; j++) {
                if (board[i][j] != '.') {
                    numMapRow[board[i][j] - '1']++;
                    if (numMapRow[board[i][j] - '1'] > 1) {
                        return false;
                    }
                }
                if (board[j][i] != '.') {
                    numMapCol[board[j][i] - '1']++;
                    if (numMapCol[board[j][i] - '1'] > 1) {
                        return false;
                    }
                }
            }
        }
        for (int i = 1; i < 9; i += 3) {
            for (int j = 1; j < 9; j += 3) {
                int[] numMapRow = new int[9];
                if (board[i][j] != '.') {
                    numMapRow[board[i][j] - '1']++;
                    if (numMapRow[board[i][j] - '1'] > 1) {
                        return false;
                    }
                }
                if (board[i + 1][j] != '.') {
                    numMapRow[board[i + 1][j] - '1']++;
                    if (numMapRow[board[i + 1][j] - '1'] > 1) {
                        return false;
                    }
                }
                if (board[i - 1][j] != '.') {
                    numMapRow[board[i - 1][j] - '1']++;
                    if (numMapRow[board[i - 1][j] - '1'] > 1) {
                        return false;
                    }
                }
                if (board[i][j - 1] != '.') {
                    numMapRow[board[i][j - 1] - '1']++;
                    if (numMapRow[board[i][j - 1] - '1'] > 1) {
                        return false;
                    }
                }
                if (board[i][j + 1] != '.') {
                    numMapRow[board[i][j + 1] - '1']++;
                    if (numMapRow[board[i][j + 1] - '1'] > 1) {
                        return false;
                    }
                }
                if (board[i - 1][j - 1] != '.') {
                    numMapRow[board[i - 1][j - 1] - '1']++;
                    if (numMapRow[board[i - 1][j - 1] - '1'] > 1) {
                        return false;
                    }
                }

                if (board[i - 1][j + 1] != '.') {
                    numMapRow[board[i - 1][j + 1] - '1']++;
                    if (numMapRow[board[i - 1][j + 1] - '1'] > 1) {
                        return false;
                    }
                }
                if (board[i + 1][j - 1] != '.') {
                    numMapRow[board[i + 1][j - 1] - '1']++;
                    if (numMapRow[board[i + 1][j - 1] - '1'] > 1) {
                        return false;
                    }
                }
                if (board[i + 1][j + 1] != '.') {
                    numMapRow[board[i + 1][j + 1] - '1']++;
                    if (numMapRow[board[i + 1][j + 1] - '1'] > 1) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    public static void main(String[] args) {
        char[][] board = {{'5','3','.','.','7','.','.','.','.'},{'6','.','.','1','9','5','.','.','.'},{'.','9','8','.','.','.','.','6','.'},{'8','.','.','.','6','.','.','.','3'},{'4','.','.','8','.','3','.','.','1'},{'7','.','.','.','2','.','.','.','6'},{'.','6','.','.','.','.','2','8','.'},{'.','.','.','4','1','9','.','.','5'},{'.','.','.','.','8','.','.','7','9'}};
        day190906 day190906 = new day190906();
        day190906.isValidSudoku(board);
    }

}
