package day190311;

/**
 * @author LI DUO
 * @date 2019/3/11 下午 05:17
 */
public class MinStack {

    /** initialize your data structure here. */

    private int[] objects;

    private int count;

    private int size = 10;

    private int min = Integer.MAX_VALUE;

    public MinStack() {
        objects = new int[size];
        count = -1;
    }

    public void push(int x) {
        checkSize();
        objects[++ count] = x;
        min = Math.min(min, x);
    }

    private void checkSize() {
        if (count == (size - 1)) {
            int[] temp = objects;
            size += 10;
            objects = new int[size];
            System.arraycopy(temp, 0, objects, 0, size - 10);
        }
    }

    public void pop() {
        if (count >= 0) {
            if (objects[count --] == min) {
                findMin();
            }
        }
    }

    private void findMin() {
        min = Integer.MAX_VALUE;
        for (int x = 0; x <= count; x++) {
            int i = objects[x];
            min = Math.min(min, i);
        }
    }

    public int top() {
        return objects[count];
    }

    public int getMin() {
        return min;
    }

    public static void main(String[] args) {
        MinStack minStack = new MinStack();
        minStack.push(-2);
        minStack.push(0);
        minStack.push(-3);
        int min = minStack.getMin();//--> 返回 -3.
        minStack.pop();
        int top = minStack.top();//--> 返回 0.
        int minStackMin = minStack.getMin();
        minStack.push(-2);
        minStack.push(0);
        minStack.push(-3);
        minStack.pop();
        int top1 = minStack.top();//--> 返回 0.
    }

}
