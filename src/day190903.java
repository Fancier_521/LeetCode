/**
 * @author LI DUO
 * @version 1.0
 * @date 2019/9/3 上午 11:36
 */
public class day190903 {

    public void reverseString(char[] s) {
        if (s.length == 0 || s.length == 1) {
            return;
        }
        int head = 0;
        int tail = s.length - 1;
        while (head < tail) {
            char temp = s[head];
            s[head] = s[tail];
            s[tail] = temp;
            head++;
            tail--;
        }
    }

    public int avg(int x, int y) {
        return (x & y) + ((x ^ y) >> 1);
    }

    public int avg2(int x, int y) {
        return (x >> 1) + (y >> 1);
    }

    public static void main(String[] args) {
        int x = 13;
        int y = 25;
        day190903 day190903 = new day190903();
//        int avg = day190903.avg(x, y);
//        System.out.println(avg);
//        int avg2 = day190903.avg2(x, y);
//        System.out.println(avg2);
//
//        String reverseWords = day190903.reverseWords("a good   example");
//        System.out.println(reverseWords);

        System.out.println(day190903.myAtoi("-+2"));
    }

    public String reverseWords(String s) {
        if (s.length() == 0) {
            return s;
        }
        StringBuilder result = new StringBuilder();
        for (int i = s.length() - 1; i >= 0; ) {
            int end = 0;
            if (s.charAt(i) == ' ') {
                i--;
                continue;
            }
            end = i + 1;
            while ((i >= 0) && s.charAt(i) != ' ') {
                i--;
            }
            result.append(s, i + 1, end);
            if (i > 0) {
                result.append(" ");
            }
        }
        return result.toString().trim();
    }

    public int myAtoi(String str) {
        if (str == null || str.length() == 0 || str.trim().length() == 0) {
            return 0;
        }
        String trimStr = str.trim();
        boolean isNegative = false;
        StringBuilder resultNum = new StringBuilder();
        int i = 0;
        boolean plus = false;
        boolean sub = false;
        while (i < trimStr.length()) {
            if (trimStr.charAt(i) == '+') {
                isNegative = false;
                i++;
                plus = true;
                if (sub) {
                    return 0;
                }
                continue;
            }
            if (trimStr.charAt(i) == '-') {
                isNegative = true;
                i++;
                sub = true;
                if (plus) {
                    return 0;
                }
                continue;
            }
            if (trimStr.charAt(i) == '.') {
                break;
            }
            if (trimStr.charAt(i) >= '0' && trimStr.charAt(i) <= '9') {
                resultNum.append(trimStr.charAt(i));
                if (resultNum.length() > 10) {
                    Long aLong = Long.valueOf(resultNum.toString());
                    if (aLong.toString().length() > 10) {
                        return isNegative ? Integer.MIN_VALUE : Integer.MAX_VALUE;
                    } else {
                        resultNum = new StringBuilder(aLong.toString());
                    }
                }
            } else {
                if (resultNum.length() == 0) {
                    return 0;
                }
                break;
            }
            i++;
        }
        if (resultNum.length() == 0) {
            return 0;
        }
        Long aLong = Long.valueOf(resultNum.toString());
        if (!isNegative && aLong > Integer.MAX_VALUE) {
            return Integer.MAX_VALUE;
        }
        if (isNegative && (-aLong) < Integer.MIN_VALUE) {
            return Integer.MIN_VALUE;
        }
        return isNegative ? -aLong.intValue() : aLong.intValue();
    }

    public int myAtoi2(String str) {
        str = str.trim();
        if (str.length() == 0) {
            return 0;
        }
        long sum = 0;
        int first = 0, n = str.length();
        int flag = 1;
        if (str.charAt(0) == '+') {
            first++;
        } else if (str.charAt(0) == '-') {
            first++;
            flag = -1;
        }
        for (int i = first; i < n; i++) {
            if (!Character.isDigit(str.charAt(i))) {
                return (int) sum * flag;
            }
            sum = 10 * sum + str.charAt(i) - '0';
            if (flag == 1 && sum > Integer.MAX_VALUE) {
                return Integer.MAX_VALUE;
            }
            if (flag == -1 && (-1) * sum < Integer.MIN_VALUE) {
                return Integer.MIN_VALUE;
            }
        }
        return (int) sum * flag;
    }

}
