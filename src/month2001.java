import java.util.*;
import java.util.function.ToLongFunction;

/**
 * @author LI DUO
 * @version 1.0
 * @date 2020/1/2 下午 03:18
 */
public class month2001 {

    static class day0102 {
        public char nextGreatestLetter(char[] letters, char target) {
            if (letters[0] > target || letters[letters.length - 1] <= target) {
                return letters[0];
            }
            int low = 0;
            int high = letters.length - 1;
            while (low < high) {
                int mid = (low + high) >>> 1;
                if (letters[mid] <= target) {
                    low = mid + 1;
                } else {
                    high = mid;
                }
            }
            return letters[high];
        }
    }

    static class contest170 {
        public String freqAlphabets(String s) {
            StringBuilder ret = new StringBuilder();
            if (s.contains("#")) {
                while (s.contains("#")) {
                    int flag = s.indexOf("#");
                    int charInt = Integer.parseInt(s.substring(flag - 2, flag));
                    s = s.replaceFirst(s.substring(flag - 2, flag + 1), String.valueOf((char) (charInt - 1 + 'a')));
                }
                for (int i = 0; i < s.length(); i++) {
                    if (s.charAt(i) >= 'a' && s.charAt(i) <= 'z') {
                        ret.append(s.charAt(i));
                        continue;
                    }
                    ret.append((char) (s.charAt(i) - '0' - 1 + 'a'));
                }
            } else {
                for (int i = 0; i < s.length(); i++) {
                    ret.append((char) (s.charAt(i) - '0' - 1 + 'a'));
                }
            }
            return ret.toString();
        }

        public int[] xorQueries(int[] arr, int[][] queries) {
            int[] ret = new int[queries.length];
            int index = 0;
            for (int[] query : queries) {
                if (query[0] == query[1]) {
                    ret[index++] = arr[query[0]];
                    continue;
                }
                int temp = arr[query[0]];
                for (int i = query[0] + 1; i <= query[1]; i++) {
                    temp = temp ^ arr[i];
                }
                ret[index++] = temp;
            }
            return ret;
        }

        //异或有个性质：a ^ b ^ a = b，因此这个题我们可以用前缀和来解决区间查询问题
        public int[] xorQueries2(int[] arr, int[][] queries) {
            int[] sum = new int[arr.length + 1];
            for (int i = 0; i < arr.length; i++) {
                sum[i + 1] = sum[i] ^ arr[i];
            }
            int[] res = new int[queries.length];
            for (int i = 0; i < queries.length; i++) {
                int[] q = queries[i];
                res[i] = sum[q[1] + 1] ^ sum[q[0]];
            }
            return res;
        }

        public List<String> watchedVideosByFriends(List<List<String>> watchedVideos, int[][] friends, int id, int level) {
            HashSet<Integer> tempFriend = new HashSet<>();
            HashSet<Integer> fFriend = new HashSet<>();
            fFriend.add(id);
            for (int i = 0; i < level; i++) {
                tempFriend.addAll(fFriend);
                HashSet<Integer> temp = new HashSet<>();
                fFriend.forEach(f -> {
                    for (int j : friends[f]) {
                        if (!tempFriend.contains(j)) {
                            temp.add(j);
                        }
                    }
                });
                fFriend = temp;
            }
            Map<String, Long> videoCounts = new HashMap<>();
            fFriend.forEach(friend -> {
                List<String> strings = watchedVideos.get(friend);
                for (String string : strings) {
                    videoCounts.put(string, videoCounts.getOrDefault(string, 0L) + 1L);
                }
            });
            List<String> videos = new ArrayList<>(videoCounts.keySet());
            videos.sort(Comparator.comparingLong((ToLongFunction<String>) videoCounts::get).thenComparing(value -> value));
            return videos;
        }

        public int minInsertions(String s) {
            int[][] dp = new int[s.length()][s.length()];
            for (int i = s.length() - 1; i >= 0; i--) {
                dp[i][i] = 1;
                for (int j = i + 1; j < s.length(); j++) {
                    if (s.charAt(i) == s.charAt(j)) {
                        dp[i][j] = dp[i + 1][j - 1] + 2;
                    } else {
                        dp[i][j] = Math.max(dp[i + 1][j], dp[i][j - 1]);
                    }
                }
            }
            return s.length() - dp[0][s.length() - 1];
        }
    }


    static class contest171 {
        public int[] getNoZeroIntegers(int n) {
            int[] ret = new int[2];
            for (int i = 1; i < n; i++) {
                int one = n - i;
                if (!String.valueOf(i).contains("0") && !String.valueOf(one).contains("0")) {
                    return new int[]{i, one};
                }
            }
            return ret;
        }

        public int minFlips(int a, int b, int c) {
            int ret = 0;
            while (a != 0 || b != 0 || c != 0) {
                int j = a & 1, k = b & 1, l = c & 1;
                if ((j | k) != l) {
                    if (j == 1 && k == 1) {
                        ret += 2;
                    } else {
                        ret += 1;
                    }
                }
                a >>= 1;
                b >>= 1;
                c >>= 1;
            }
            return ret;
        }

        public int makeConnected(int n, int[][] connections) {
            if (n - 1 > connections.length) {
                return -1;
            }
            List<Integer>[] graphs = new ArrayList[n];
            for (int i = 0; i < n; i++) {
                graphs[i] = new ArrayList<>();
            }
            for (int[] c : connections) {
                graphs[c[0]].add(c[1]);
                graphs[c[1]].add(c[0]);
            }
            int[] book = new int[n];
            Arrays.fill(book, -1);
            int color = 0;
            for (int i = 0; i < n; i++) {
                if (book[i] != -1 || graphs[i].size() == 0) {
                    continue;
                }
                dfs(i, graphs, color++, book);
            }
            int res = 0;
            Set<Integer> set = new HashSet<>(n);
            for (int i : book) {
                if (i == -1) {
                    res++;
                } else if (!set.contains(i)) {
                    res++;
                    set.add(i);
                }
            }
            return res - 1;
        }

        private void dfs(int cur, List<Integer>[] graphs, final int color, int[] book) {
            book[cur] = color;
            for (int next : graphs[cur]) {
                if (book[next] != -1) {
                    continue;
                }
                dfs(next, graphs, color, book);
            }
        }

        private final static Map<Character, int[]> CHARACTER_MAP;

        private static char placeholder = '#';

        static {
            CHARACTER_MAP = new HashMap<>(26);
            for (int i = 0; i < 26; i++) {
                CHARACTER_MAP.put((char) (i + 'A'), new int[]{i / 6, i % 6});
            }
        }

        public int minimumDistance(String word) {
            char[] charArray = word.toCharArray();
            Map<String, Integer> dp = new HashMap<>();
            return helper(0, charArray, placeholder, placeholder, dp);
        }

        private int helper(int index, char[] charArray, char left, char right, Map<String, Integer> dp) {
            if (index == charArray.length) {
                return 0;
            }
            String key = left + right + "," + index;
            if (dp.containsKey(key)) {
                return dp.get(key);
            }
            int res = Math.min(getDistance(left, charArray[index]) + helper(index + 1, charArray, charArray[index], right, dp),
                    getDistance(right, charArray[index]) + helper(index + 1, charArray, left, charArray[index], dp));
            dp.put(key, res);
            return res;
        }

        private int getDistance(char a, char b) {
            if (a == placeholder || b == placeholder) {
                return 0;
            }
            int[] x1 = CHARACTER_MAP.get(a);
            int[] x2 = CHARACTER_MAP.get(b);
            return Math.abs(x1[0] - x2[0]) + Math.abs(x1[1] - x2[1]);
        }
    }

    static class sf2020 {

        Set<String> stringSet = new HashSet<String>();

        private String[] zero = new String[]{"1", "2", "4"};

        private String[] two = new String[]{"3", "5"};

        public void generate() {
            StringBuilder builder = new StringBuilder("");
            for (String s1 : two) {
                builder.append(s1);
                for (String s2 : zero) {
                    builder.append(s2);
                    for (String s3 : two) {
                        builder.append(s3);
                        for (String s4 : zero) {
                            builder.append(s4);
                            for (String s5 : two) {
                                builder.append(s5);
                                for (String s6 : zero) {
                                    builder.append(s6);
                                    for (String s7 : two) {
                                        builder.append(s7);
                                        for (String s8 : zero) {
                                            builder.append(s8);
                                            for (String s9 : two) {
                                                builder.append(s9);
                                                for (String s10 : zero) {
                                                    builder.append(s10);
                                                    for (String s11 : two) {
                                                        builder.append(s11);
                                                        for (String s12 : zero) {
                                                            builder.append(s12);
                                                            stringSet.add(builder.toString());
//                                                            System.out.println(builder.toString());
                                                            builder.deleteCharAt(11);
                                                        }
                                                        builder.deleteCharAt(10);
                                                    }
                                                    builder.deleteCharAt(9);
                                                }
                                                builder.deleteCharAt(8);
                                            }
                                            builder.deleteCharAt(7);
                                        }
                                        builder.deleteCharAt(6);
                                    }
                                    builder.deleteCharAt(5);
                                }
                                builder.deleteCharAt(4);
                            }
                            builder.deleteCharAt(3);
                        }
                        builder.deleteCharAt(2);
                    }
                    builder.deleteCharAt(1);
                }
                builder.deleteCharAt(0);
            }
            stringSet.forEach(System.out::println);
        }

        public String q() {
            return this.q();
        }
    }

    static class contest172 {
        public int maximum69Number(int num) {
            String valueOf = String.valueOf(num);
            char[] chars = valueOf.toCharArray();
            for (int i = 0; i < chars.length; i++) {
                char aChar = chars[i];
                if (aChar == '6') {
                    chars[i] = 9;
                    break;
                }
            }
            return Integer.parseInt(String.valueOf(chars));
        }

        public List<String> printVertically(String s) {
            String[] words = s.split(" ");
            Map<Integer, String> stringMap = new HashMap<>();
            List<String> ans = new ArrayList<>();
            int maxLen = 0, verticalPos = 0;
            for (String word : words) {
                maxLen = Math.max(word.length(), maxLen);
                for (int i = 0; i < word.length(); i++) {
                    StringBuilder cur = new StringBuilder(stringMap.getOrDefault(i, ""));
                    while (cur.length() < verticalPos) {
                        cur.append(" ");
                    }
                    stringMap.put(i, cur.toString() + word.charAt(i));
                }
                verticalPos++;
            }
            for (int i = 0; i < maxLen; i++) {
                ans.add(stringMap.get(i));
            }
            return ans;
        }

        public class TreeNode {
            int val;
            TreeNode left;
            TreeNode right;

            TreeNode(int x) {
                val = x;
            }
        }

        public TreeNode removeLeafNodes(TreeNode root, int target) {
            if (root == null) {
                return null;
            }
            TreeNode left = removeLeafNodes(root.left, target);
            TreeNode right = removeLeafNodes(root.right, target);
            // leaf
            if (left == null && right == null & root.val == target) {
                return null;
            }
            root.left = left;
            root.right = right;
            return root;
        }

        static class Range {
            private int left;

            private int right;

            public Range() {
            }
        }

        public int minTaps(int n, int[] ranges) {
            Range[] rangesArr = new Range[n + 1];
            for (int i = 0; i <= n; i++) {
                rangesArr[i] = new Range();
                rangesArr[i].left = Math.max(0, i - ranges[i]);
                rangesArr[i].right = Math.min(n, i + ranges[i]);
            }
            Arrays.sort(rangesArr, (rangeA, rangeB) -> {
                if (rangeA.left != rangeB.left) {
                    return Integer.compare(rangeA.left, rangeB.left);
                } else {
                    return Integer.compare(rangeA.right, rangeB.right);
                }
            });
            int leftmost = 0, ret = 0, rightMost = 0;
            for (int i = 0; i <= n; i++) {
                if (leftmost >= rangesArr[i].left) {
                    rightMost = Math.max(rightMost, rangesArr[i].right);
                } else {
                    if (rightMost > leftmost) {
                        leftmost = rightMost;
                        rightMost = 0;
                        ret++;
                        i--;
                    } else {
                        return -1;
                    }
                }
            }
            if (rightMost > leftmost) {
                ret++;
            }
            return ret;
        }
    }

    public static void main(String[] args) {
        contest172 contest172 = new contest172();
        contest172.minTaps(5, new int[]{3, 4, 1, 1, 0, 0});
    }
}
