/**
 * @author LI DUO
 * @version 1.0
 * @date 2020/3/1 上午 10:41
 */
public class ListNode {
    int val;
    ListNode next;

    ListNode(int x) {
        val = x;
    }
}
