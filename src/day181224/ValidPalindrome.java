package day181224;

/**
 * @author LI DUO
 * @date 2018/12/24 下午 03:59
 */
public class ValidPalindrome {
    public boolean isPalindrome(String s) {
        if (s == null) {
            return false;
        }
        StringBuilder builder = new StringBuilder();
        StringBuilder reverse = new StringBuilder();
        char[] chars = s.toCharArray();
        for (char c : chars) {
            if (Character.isLetterOrDigit(c)) {
                builder.append(c);
            }
        }
        for (int i = chars.length - 1; i >= 0; i--) {
            if (Character.isLetterOrDigit(chars[i])) {
                reverse.append(chars[i]);
            }
        }
        if (builder.toString().equalsIgnoreCase(reverse.toString())) {
            return true;
        }
        return false;
    }

    boolean isPalindrome2(String s) {
        if (s == null) {
            return false;
        }
        return false;
    }

    public static void main(String[] args) {
        ValidPalindrome palindrome = new ValidPalindrome();
        System.out.println(palindrome.isPalindrome("A man, a plan, a canal: Panama"));
    }
}
