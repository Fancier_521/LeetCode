package day181224;

/**
 * @author LI DUO
 * @date 2018/12/24 下午 04:44
 */
public class SingleNumber {
    public int singleNumber(int[] nums) {
        if (nums == null) {
            return 0;
        }
        int search = 0;
        int length = nums.length;
        for (int i = 0; i < length; i++) {
            search ^= nums[i];
        }
        return search;
    }

    public static void main(String[] args) {
        SingleNumber number = new SingleNumber();
        int[] ints = {4,1,2,1,2};
        System.out.println(number.singleNumber(ints));
    }
}
