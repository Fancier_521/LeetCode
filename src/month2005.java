
import java.util.*;

/**
 * @author LI DUO
 * @version 1.0
 * @date 2020/5/6 18:23
 */
public class month2005 {

    public int mincostTickets(int[] days, int[] costs) {
        int[] dp = new int[days[days.length - 1] + 1];
        for (int day : days) {
            dp[day] = -1;
        }
        for (int i = 1; i < dp.length; i++) {
            if (dp[i] == 0) {
                dp[i] = dp[i - 1];
            } else {
                int one, seven, thirty;
                one = dp[i - 1] + costs[0];
                if (i - 7 >= 0) {
                    seven = dp[i - 7] + costs[1];
                } else {
                    seven = costs[1];
                }
                if (i - 30 >= 0) {
                    thirty = dp[i - 30] + costs[2];
                } else {
                    thirty = costs[2];
                }
                dp[i] = Math.min(one, seven);
                dp[i] = Math.min(dp[i], thirty);
            }
        }
        return dp[days[days.length - 1]];
    }

    public List<String> buildArray(int[] target, int n) {
        List<String> ret = new ArrayList<>();
        int index = 0;
        for (int i = 1; i <= n; i++) {
            if (i == target[index]) {
                ret.add("Push");
                index++;
            } else {
                ret.add("Push");
                ret.add("Pop");
            }
            if (index == target.length) {
                break;
            }
        }
        return ret;
    }

    public int countTriplets(int[] arr) {
        int length = arr.length;
        int count = 0;
        for (int i = 0; i < length; i++) {
            for (int j = i + 1; j < length; j++) {
                int numA = getNum(arr, i, j, false);
                for (int k = j; k < length; k++) {
                    int numB = getNum(arr, j, k, true);
                    if (numA == numB) {
                        count++;
                        System.out.println(i + "," + j + "," + k);
                    }
                }
            }
        }
        return count;
    }

    private int getNum(int[] arr, int b, int e, boolean flag) {
        int ret = arr[b];
        for (int i = b + 1; flag ? i <= e : i < e; i++) {
            ret ^= arr[i];
        }
        return ret;
    }

//    public int minTime(int n, int[][] edges, List<Boolean> hasApple) {
//        if (!hasApple.contains(Boolean.TRUE)) {
//            return 0;
//        }
//        if (!hasApple.contains(Boolean.FALSE)) {
//            return edges.length * 2;
//        }
//        Map<Integer, List<Integer>> listMap = new HashMap<>(n);
//        for (int[] edge : edges) {
//            List<Integer> list = listMap.getOrDefault(edge[0], new ArrayList<>());
//            list.add(edge[1]);
//            listMap.put(edge[0], list);
//        }
//        Queue<Integer> queue = new LinkedList<>();
//        queue.offer(0);
//        while (!queue.isEmpty()) {
//            Integer peek = queue.peek();
//            List<Integer> integerList = listMap.get(peek);
//
//        }
//    }

    public int busyStudent(int[] startTime, int[] endTime, int queryTime) {
        int count = 0;

        for (int i = 0; i < startTime.length; i++) {
            if (queryTime >= startTime[i] || queryTime <= endTime[i]) {
                count++;
            }
        }

        return count;
    }

    public String arrangeWords(String text) {
        String[] strings = text.split(" ");
        HashMap<Integer, List<Integer>> listHashMap = new HashMap<>(text.length());
        for (int i = 0; i < strings.length; i++) {
            String string = strings[i];
            List<Integer> orDefault = listHashMap.getOrDefault(string.length(), new ArrayList<>());
            orDefault.add(i);
            listHashMap.put(string.length(), orDefault);
        }
        TreeMap<Integer, List<Integer>> treeMap = new TreeMap<>(listHashMap);
        StringBuilder builder = new StringBuilder();
        for (Map.Entry<Integer, List<Integer>> entry : treeMap.entrySet()) {
            Integer key = entry.getKey();
            System.out.println(key);
            List<Integer> value = entry.getValue();
            for (Integer index : value) {
                String string = strings[index].toLowerCase();
                builder.append(string).append(" ");
            }
        }
        char charAt = Character.toUpperCase(builder.charAt(0));
        return builder.replace(0, 1, String.valueOf(charAt)).deleteCharAt(builder.length() - 1).toString();
    }

    public List<Integer> peopleIndexes(List<List<String>> favoriteCompanies) {
        List<Integer> ret = new ArrayList<>();
        if (favoriteCompanies.size() == 1) {
            ret.add(0);
            return ret;
        }
        Set<Integer> hashSet = new TreeSet<>();
        favoriteCompanies.forEach(Collections::sort);
        // favoriteCompanies.sort(Comparator.comparingInt(List::size));
        for (int i = 0; i < favoriteCompanies.size(); i++) {
            List<String> favoriteCompany = favoriteCompanies.get(i);
            hashSet.add(i);
            // System.out.println(Arrays.toString(favoriteCompany.toArray()));
            for (int j = 0; j < favoriteCompanies.size(); j++) {
                if (j == i) {
                    continue;
                }
                List<String> list = favoriteCompanies.get(j);
                HashSet<String> stringHashSet = new HashSet<>(list);
                if (stringHashSet.containsAll(favoriteCompany)) {
                    hashSet.remove(i);
                    break;
                }
            }
        }
        ret.addAll(hashSet);
        return ret;
    }

    class Point {
        double x, y;

        Point(double x, double y) {
            this.x = x;
            this.y = y;
        }
    }

    private final double eps = 1e-8;

    public int numPoints(int[][] points, int r) {
        Point[] pointsArr = new Point[101];
        for (int i = 0; i < points.length; i++) {
            pointsArr[i] = new Point(points[i][0], points[i][1]);
        }
        int ans = 1;
        for (int i = 0; i < points.length; i++) {
            for (int j = i + 1; j < points.length; j++) {
                if (dist(pointsArr[i], pointsArr[j]) > 2.0 * r) {
                    continue;
                }
                Point center = getCircle(pointsArr[i], pointsArr[j], r);
                int count = 0;
                for (int k = 0; k < points.length; k++) {
                    if (dist(center, pointsArr[k]) < 1.0 * r + eps) {
                        count++;
                    }
                }
                ans = Math.max(ans, count);
            }
        }
        return ans;
    }

    private double dist(Point p1, Point p2) {
        return Math.sqrt((p1.x - p2.x) * (p1.x - p2.x) + (p1.y - p2.y) * (p1.y - p2.y));
    }

    private Point getCircle(Point p1, Point p2, int r) {
        Point mid = new Point((p1.x + p2.x) / 2, (p1.y + p2.y) / 2);
        double angle = Math.atan2(p1.x - p2.x, p2.y - p1.y);
        double d = Math.sqrt(r * r - Math.pow(dist(p1, mid), 2));
        return new Point(mid.x + d * Math.cos(angle), mid.y + d * Math.sin(angle));
    }

    public int isPrefixOfWord(String sentence, String searchWord) {
        int ret = -1;
        String[] strings = sentence.split(" ");
        for (int i = 0; i < strings.length; i++) {
            String string = strings[i];
            if (string.startsWith(searchWord)) {
                return i + 1;
            }
        }
        return ret;
    }

    public int maxVowels(String s, int k) {
        int ret = 0;
        HashSet<Character> characters = new HashSet<>(Arrays.asList('a', 'e', 'i', 'o', 'u'));
        char[] chars = s.toCharArray();
        int j = 0, max = 0;
        for (int i = 0; i < chars.length; i++) {
            if (characters.contains(chars[i])) {
                max++;
            }
            if (i > k - 1) {
                if (!characters.contains(chars[j])) {
                    max--;
                }
                j++;
            }
            ret = Math.max(ret, max);
            if (ret == k) {
                return ret;
            }
        }
        return ret;
    }


    public static void main(String[] args) {
        month2005 month2005 = new month2005();
        System.out.println(month2005.maxVowels("ibpbhixfiouhdljnjfflpapptrxgcomvnb", 33));
    }
}