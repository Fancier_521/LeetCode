package day180130;

/**
 * author: 李铎
 * date: 2018/1/30
 **/
public class Sqrt {
    public static int mySqrt(int x) {
        double result = Math.sqrt(x);
        return (int) result;
    }

    public static void main(String[] args) {
        System.out.println(mySqrt(8));
    }
}
