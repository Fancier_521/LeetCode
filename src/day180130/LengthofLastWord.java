package day180130;

/**
 * author: 李铎
 * date: 2018/1/30
 **/
public class LengthofLastWord {
    public static int lengthOfLastWord(String s) {
        s = s.trim();
        if ("".equals(s))
            return 0;
        char[] chars = s.toCharArray();
        int length = 0;
        boolean onlyAWord = false;
        for (int i = chars.length - 1; i >= 0; i--){
            length ++;
            if (chars[i]==' '){
                onlyAWord = false;
                break;
            }
            onlyAWord = true;
        }
        return onlyAWord?length:length - 1;
    }

    public static void main(String[] args) {
        String s = "world ";
        int lengthOfLastWord = lengthOfLastWord(s);
        System.out.println(lengthOfLastWord);
    }
}
