package day180130;

/**
 * author: 李铎
 * date: 2018/1/30
 **/
public class AddBinary {
    public static String addBinary(String a, String b) {
        StringBuilder builder = new StringBuilder();
        int i = a.length() - 1, j = b.length() - 1, sum = 0;
        while (i >= 0 || j >= 0){
            sum /= 2;
            if (i >= 0)
                sum += a.charAt(i--) - '0';
            if (j >= 0)
                sum += b.charAt(j--) - '0';
            builder.append(sum % 2);
        }
        if (sum / 2 != 0)
            builder.append(1);
        return builder.reverse().toString();
    }

    public static void main(String[] args) {
        System.out.println(addBinary("110", "1"));
    }
}
