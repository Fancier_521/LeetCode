package day180130;

import java.util.Arrays;

/**
 * author: 李铎
 * date: 2018/1/30
 **/
public class PlusOne {
    public static int[] plusOne(int[] digits){
        for (int i = digits.length - 1; i >= 0; i--){
            if (digits[i] < 9){
                digits[i]++;
                return digits;
            }
            digits[i] = 0;
        }

        int[] result = new int[digits.length + 1];
        result[0] = 1;
        return result;
    }

    public static void main(String[] args) {
        int a[] = {9,9,9};
        int[] plusOne = plusOne(a);
        System.out.println(Arrays.toString(plusOne));
    }
}
