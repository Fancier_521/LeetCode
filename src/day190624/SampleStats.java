package day190624;

/**
 * @author LI DUO
 * @version 1.0
 * @date 2019/6/24 下午 04:14
 * 1093. 大样本统计
 * https://leetcode-cn.com/problems/statistics-from-a-large-sample/comments/
 */
public class SampleStats {
    public double[] sampleStats(int[] count) {
        double minValue = Double.MAX_VALUE;
        double maxValue = Double.MIN_VALUE;
        double avgValue = 0.0;
        double midValue = 0.0;
        double mostValue = 0.0;
        double sum = 0.0;
        int mostIndex = Integer.MIN_VALUE;
        int sumNumber = 0;
        for (int i = 0; i < count.length; i++) {
            if (count[i] == 0) {
                continue;
            }
            int number = count[i];
            minValue = Double.min(i, minValue);
            maxValue = Double.max(i, maxValue);
            if (number > mostIndex) {
                mostValue = i;
                mostIndex= number;
            }
            sum += number * i;
            sumNumber += number;
        }
        avgValue = sum / (double) sumNumber;
        int midL, midR = 0;
        if (sumNumber % 2 == 0) {
            midL = sumNumber / 2;
            midR = sumNumber / 2 + 1;
        } else {
            midL = sumNumber / 2 + 1;
            midR = sumNumber / 2 + 1;
        }

        for (int i = 0; i < count.length; i++) {
            if (count[i] == 0) {
                continue;
            }
            midL -= count[i];
            midR -= count[i];
            if (midL <= 0) {
                midValue += i;
                midL = Integer.MAX_VALUE;
            }
            if (midR <= 0) {
                midValue += i;
                break;
            }
        }
        return new double[]{minValue, maxValue, avgValue, midValue / 2.0, mostValue};
    }
}
