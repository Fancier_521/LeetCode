import java.util.Arrays;

/**
 * @author LI DUO
 * @version 1.0
 * @date 2019/9/11 下午 05:31
 */
public class day190911 {

    public int maxArea(int[] height) {
        if (height.length == 0) {
            return 0;
        }
        int maxArea = Integer.MIN_VALUE;
        int head = 0;
        int tail = height.length - 1;
        while (head < height.length - 1) {
            int minSide = Math.min(height[head], height[tail]);
            maxArea = Math.max(maxArea, (tail - head) * minSide);
            if (height[head] > height[tail]) {
                tail --;
            } else {
                head ++;
            }
        }
        return maxArea;
    }

    public int threeSumClosest(int[] nums, int target) {
        if (nums.length < 3) {
            return target;
        }
        Arrays.sort(nums);
        int closetNum = nums[0] + nums[1] + nums[2];
        for (int i = 0; i < nums.length - 2; i++) {
            int l = i + 1;
            int r = nums.length - 1;
            while (l < r) {
                int thisSum = nums[l] + nums[r] + nums[i];
                if (Math.abs(thisSum - target) < Math.abs(closetNum - target)) {
                    closetNum = thisSum;
                }
                if (thisSum > target) {
                    r--;
                } else if (thisSum < target) {
                    l++;
                } else {
                    return thisSum;
                }
            }
        }
        return closetNum;
    }

}
