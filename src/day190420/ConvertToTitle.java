package day190420;

/**
 * @Author: LI DUO
 * @Date: 2019/4/20 下午 05:46
 * @Version 1.0
 */
public class ConvertToTitle {
    public String convertToTitle(int n) {
        String result = "";
        while (n > 0) {
            n--;
            result= ((char) (n % 26 + 'A')) + result;
            n /= 26;
        }
        return result;
    }

    public static void main(String[] args) {
        ConvertToTitle convertToTitle = new ConvertToTitle();
        String s = convertToTitle.convertToTitle(28);
        System.out.println(s);
    }
}
