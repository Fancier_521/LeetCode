package day190823;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

/**
 * @author LI DUO
 * @version 1.0
 * @date 2019/8/23 下午 05:59
 */
public class CheckRecord {
    int M = (int) 1e9 + 7;

    public int checkRecord(int n) {
        if (n == 0) {
            return 1;
        }
        //最新记录为P,且没有A      PS:A = P;
        long P = 1;
        //最新记录为P,且有A
        long AP = 0;
        //最新记录为L,且没有A
        long L = 1;
        //最新记录为LL,且没有A
        long LL = 0;
        //最新记录为L,且有A
        long AL = 0;
        //最新记录为LL,且有A
        long ALL = 0;
        //最新记录为A
        long A = 1;

        for (int i = 2; i <= n; i++) {
            //#如果第i次记录是P
            long p = (P + L + LL) % M;
            //#如果第i次记录是P
            long ap = (AP + AL + ALL + A) % M;
            //#如果第i次记录是L
            long l = P;
            //#如果第i次记录是L
            long ll = L;
            //#如果第i次记录是L
            long al = (AP + A) % M;
            //#如果第i次记录是L
            long all = AL;
            //#如果第i次记录是A
            long a = (P + L + LL) % M;

            P = p;
            AP = ap;
            L = l;
            LL = ll;
            AL = al;
            ALL = all;
            A = a;
        }
        return Math.toIntExact((P + AP + L + LL + AL + ALL + A) % M);
    }

    public List<String> invalidTransactions(String[] transactions) {
        if (transactions.length <= 0) {
            return new ArrayList<>();
        }
        HashSet<String> invalidTrans = new HashSet<>();
        List<Transcation> trans = new ArrayList<>();
        for (String transaction : transactions) {
            String[] split = transaction.split(",");
            String name = split[0];
            Integer time = Integer.valueOf(split[1]);
            Integer amount = Integer.valueOf(split[2]);
            String city = split[3];
            Transcation transcation = new Transcation(name, time, amount, city);
            trans.add(transcation);
        }
        for (int i = 0; i < trans.size(); i++) {
            Transcation tran = trans.get(i);
            if (tran.getAmount() > 1000) {
                invalidTrans.add(tran.toString());
                continue;
            }
            for (int j = 0; j < trans.size(); j++) {
                if (i == j) {
                    continue;
                }
                Transcation tranJ = trans.get(j);
                if ((!tranJ.getCity().equals(tran.getCity())) && tranJ.getName().equals(tran.getName()) && (Math.abs(tranJ.getTime() - tran.getTime()) <= 60)) {
                    invalidTrans.add(tran.toString());
                }
            }
        }
        return new ArrayList<>(invalidTrans);
    }

    class Transcation {
        String name;

        int time;

        int amount;

        String city;

        public Transcation(String name, int time, int amount, String city) {
            this.name = name;
            this.time = time;
            this.amount = amount;
            this.city = city;
        }

        public Transcation() {
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getTime() {
            return time;
        }

        public void setTime(int time) {
            this.time = time;
        }

        public int getAmount() {
            return amount;
        }

        public void setAmount(int amount) {
            this.amount = amount;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        @Override
        public String toString() {
            return "" + name +
                    "," + time +
                    "," + amount +
                    "," + city;
        }
    }

    public int[] numSmallerByFrequency(String[] queries, String[] words) {
        int[] result = new int[queries.length];
        int[] queriesFunc = new int[queries.length];
        int[] wordsFun = new int[words.length];
        for (int i = 0; i < queries.length; i++) {
            String query = queries[i];
            queriesFunc[i] = func(query);
        }
        for (int i = 0; i < words.length; i++) {
            wordsFun[i] = func(words[i]);
        }
        for (int i = 0; i < queries.length; i++) {
            int queryF = queriesFunc[i];
            int temp = 0;
            for (int wordF : wordsFun) {
                if (wordF > queryF) {
                    temp++;
                }
            }
            result[i] = temp;
        }
        return result;
    }

    private int func(String s) {
        int[] charMap = new int[26];
        char[] chars = s.toCharArray();
        for (char aChar : chars) {
            charMap[aChar - 'a']++;
        }
        for (int i : charMap) {
            if (i != 0) {
                return i;
            }
        }
        return 0;
    }

    public static class ListNode {
        int val;
        ListNode next;

        ListNode(int x) {
            val = x;
        }
    }

    public ListNode removeZeroSumSublists(ListNode head) {
        List<Integer> integers = new ArrayList<>();
        while (head != null) {
            if (head.val == 0) {
                integers.add(Integer.MIN_VALUE);
            } else {
                integers.add(head.val);
            }
            head = head.next;
        }
        for (int i = 0; i < integers.size(); i++) {
            int temp = 0;
            boolean flag = false;
            for (int j = i; j < integers.size() && !flag; j++) {
                if (integers.get(j).equals(Integer.MIN_VALUE)) {
                    continue;
                }
                temp += integers.get(j);
                if (temp == 0) {
                    for (int k = i; k <= j; k++) {
                        integers.set(k, Integer.MIN_VALUE);
                    }
                    flag = true;
                }
            }
        }
        ListNode node = new ListNode(0);
        ListNode temp = node;
        for (Integer integer : integers) {
            if (!integer.equals(Integer.MIN_VALUE)) {
                ListNode cur = new ListNode(integer);
                temp.next = cur;
                temp = cur;
            }
        }
        return node.next;
    }

    public static void main(String[] args) {
        ListNode node = new ListNode(1);
        node.next = new ListNode(2);
        node.next.next = new ListNode(-3);
        node.next.next.next = new ListNode(3);
        node.next.next.next.next = new ListNode(1);

        CheckRecord checkRecord = new CheckRecord();
        ListNode zeroSumSublists = checkRecord.removeZeroSumSublists(node);

    }
}
