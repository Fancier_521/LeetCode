package day180201;

/**
 * author: 李铎
 * date: 2018/2/1
 **/
public class SameTree {
    public static boolean isSameTree(TreeNode p, TreeNode q) {
        return p == null && q == null || p != null && q != null
                && p.val == q.val && isSameTree(p.left, q.left)
                && isSameTree(p.right, q.right);
    }

    public static void main(String[] args) {
        TreeNode p = new TreeNode(1);
        p.left = new TreeNode(2);
        p.right = new TreeNode(1);
        TreeNode q = new TreeNode(1);
        q.left = new TreeNode(2);
        q.right = new TreeNode(1);

        System.out.println(isSameTree(p, q));
        System.out.println(System.currentTimeMillis());
    }
}
