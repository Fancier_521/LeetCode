package day180201;

import java.util.Arrays;

/**
 * author: 李铎
 * date: 2018/2/1
 **/
public class MergeSortedArray {
    public static void merge(int[] nums1, int m, int[] nums2, int n){
        int i = m - 1, j = n - 1, k = m + n -1;
        while (i >= 0 && j >= 0){
            nums1[k--] = nums1 [i] > nums2[j] ? nums1[i--] : nums2[j--];
        }
        while (j >= 0){
            nums1[k--] = nums2[j--];
        }
    }

    public static void main(String[] args) {
        int[] a = {1,3,5,6};
        int[] b = {2,4,7,9};
        merge(a,3,b,1);
        System.out.println(Arrays.toString(a));
    }
}
