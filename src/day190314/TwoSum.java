package day190314;

/**
 * @author LI DUO
 * @date 2019/3/14 下午 05:00
 */
public class TwoSum {
    public int[] twoSum(int[] numbers, int target) {
        if (numbers[0] > target) {
            return new int[2];
        }
        int left = 0, right = numbers.length - 1;
        while (left < right) {
            if (numbers[left] + numbers[right] == target) {
                return new int[]{left + 1, right + 1};
            } else if (numbers[left] + numbers[right] < target) {
                left ++;
            } else {
                right --;
            }
        }
        return new int[2];
    }

    public static void main(String[] args) {
        int[] numbers = new int[]{2, 7, 11, 15};
        TwoSum twoSum = new TwoSum();
        twoSum.twoSum(numbers, 9);

    }
}
