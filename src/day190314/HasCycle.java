package day190314;

import day190312.ListNode;

import java.util.HashSet;

/**
 * @author LI DUO
 * @date 2019/3/14 下午 04:21
 */
public class HasCycle {

    public boolean hasCycle(ListNode head) {
        if (head == null || head.next == null) {
            return false;
        }
        ListNode slow = head;
        ListNode fast = head.next;
        while (slow != fast) {
            if (fast == null || fast.next == null) {
                return false;
            }
            slow = slow.next;
            fast = fast.next.next;
        }
        return true;
    }

    boolean hasCycle2(ListNode head) {
        if (head == null || head.next == null) {
            return false;
        }
        HashSet<ListNode> nodeHashSet = new HashSet<>();
        while (head != null) {
            if (nodeHashSet.contains(head)) {
                return true;
            } else {
                nodeHashSet.add(head);
            }
            head = head.next;
        }
        return false;
    }

}
