package day180208;

/**
 * author: 李铎
 * date: 2018/2/8
 **/
public class RemoveElement {
    public static int removeElement(int[] nums, int val){
        int length = 0;
        for (int i = 0; i < nums.length; i ++ ){
            if (nums[i] != val)
                nums[length ++] = nums[i];
        }
        return length;
    }
}
