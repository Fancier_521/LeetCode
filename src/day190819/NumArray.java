package day190819;

/**
 * @author LI DUO
 * @version 1.0
 * @date 2019/8/19 下午 05:29
 */
public class NumArray {
    private int[] dpMap;
    public NumArray(int[] nums) {
        if (nums.length == 0) {
            return;
        }
        dpMap = new int[nums.length];
        dpMap[0] = nums[0];
        for (int i = 1; i < nums.length; i++) {
            dpMap[i] = dpMap[i - 1] + nums[i];
        }
    }

    public int sumRange(int i, int j) {
        if (i == 0) {
            return dpMap[j];
        } else {
            return dpMap[j] - dpMap[i - 1];
        }
    }

    public static void main(String[] args) {
        int[] nums = {-2, 0, 3, -5, 2, -1};
        NumArray numArray = new NumArray(nums);

    }

}
