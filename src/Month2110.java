/**
 * @author Li Duo
 * @version 1.0
 * @date 2021/9/30 14:41
 */
public class Month2110 {

    public int computeArea(int ax1, int ay1, int ax2, int ay2, int bx1, int by1, int bx2, int by2) {
        int aArea = (ax2 - ax1) * (ay2 - ay1);
        int bArea = (bx2 - bx1) * (by2 - by1);
        if (ax2 <= bx1 || by1 >= ay2 || ay1 >= by2 || ax1 >= bx2) {
            return aArea + bArea;
        }
        int topX = Math.min(ax2, bx2), topY = Math.min(ay2, by2);
        int bottomX = Math.max(ax1, bx1), bottomY = Math.max(ay1, by1);
        return aArea - (topX - bottomX) * (topY - bottomY) + bArea;
    }

}
