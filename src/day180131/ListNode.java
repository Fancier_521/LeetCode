package day180131;

/**
 * author: 李铎
 * date: 2018/1/31
 **/
public class ListNode {
    int val;
    ListNode next;
    ListNode(int x){
        val = x;
    }
}
