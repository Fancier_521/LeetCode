package day180131;

/**
 * author: 李铎
 * date: 2018/1/31
 **/
public class RemoveDuplicatesFromSortedList {
    public static ListNode deleteDuplicates(ListNode head){
        if(head == null || head.next == null)
            return head;
        head.next = deleteDuplicates(head.next);
        return head.val == head.next.val ? head.next : head;
    }

    public static void main(String[] args) {
        ListNode listNode = new ListNode(1);
        listNode.next = new ListNode(1);
        listNode.next.next = new ListNode(2);
        listNode.next.next.next = new ListNode(3);
        ListNode deleteDuplicates = deleteDuplicates(listNode);
        while (deleteDuplicates.next != null) {
            System.out.print(deleteDuplicates.val + "->");
            deleteDuplicates = deleteDuplicates.next;
        }
    }
}
