package day180206;

/**
 * author: 李铎
 * date: 2018/2/6
 **/
public class MaximumDepthOfBinaryTree {
    public static int maxDepth(TreeNode root){
        return root == null ? 0 : 1 + Math.max(maxDepth(root.left), maxDepth(root.right));
    }

    public static void main(String[] args) {

    }
}
