package day180206;

/**
 * author: 李铎
 * date: 2018/2/6
 **/
public class SymmetricTree {
    public static boolean isSymmetric(TreeNode root) {
        return root == null || isSymmetricHelp(root.left, root.right);
    }

    public static boolean isSymmetricHelp(TreeNode p, TreeNode q){
        if (p == null && q == null)
            return true;
        if (p == null || q == null)
            return false;
        return (p.val == q.val) && isSymmetricHelp(p.left, q.right) && isSymmetricHelp(p.right, q.left);
    }


    public static void main(String[] args) {

    }
}
