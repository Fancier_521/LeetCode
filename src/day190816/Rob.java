package day190816;

/**
 * @author LI DUO
 * @version 1.0
 * @date 2019/8/16 下午 05:06
 */
public class Rob {
    public int rob(TreeNode root) {
        if (root == null) {
            return 0;
        }
        int leftMoney = rob(root.left);
        int rightMoney = rob(root.right);
        int leftChildLeftMoney = 0;
        int leftChildRightMoney = 0;
        int rightChildLeftMoney = 0;
        int rightChildRightMoney = 0;
        if (root.left != null) {
            leftChildLeftMoney = rob(root.left.left);
            leftChildRightMoney = rob(root.left.right);
        }
        if (root.right != null) {
            rightChildLeftMoney = rob(root.right.left);
            rightChildRightMoney = rob(root.right.right);
        }

        return Math.max(leftMoney + rightMoney, root.val + leftChildLeftMoney + leftChildRightMoney + rightChildLeftMoney + rightChildRightMoney);
    }

    public class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode(int x) {
            val = x;
        }
    }
}
