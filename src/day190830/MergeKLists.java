package day190830;

import java.util.Comparator;
import java.util.Objects;
import java.util.PriorityQueue;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author LI DUO
 * @version 1.0
 * @date 2019/8/30 下午 03:20
 */
public class MergeKLists {

    public class ListNode {
        int val;
        ListNode next;

        ListNode(int x) {
            val = x;
        }
    }

    public ListNode mergeKLists(ListNode[] lists) {
        PriorityQueue<ListNode> queue = new PriorityQueue<>(Comparator.comparingInt(node -> node.val));
        queue.addAll(Stream.of(lists).filter(Objects::nonNull).collect(Collectors.toList()));
        ListNode head = new ListNode(0);
        ListNode cur = head;
        while (!queue.isEmpty()) {
            ListNode node = queue.poll();
            cur.next = new ListNode(node.val);
            cur = cur.next;
            if (node.next != null) {
                queue.add(node.next);
            }
        }
        return head.next;
    }

    public boolean canConstruct(String ransomNote, String magazine) {
        int noteLength = ransomNote.length();
        int maLength = magazine.length();
        if (noteLength > maLength) {
            return false;
        }
        if (noteLength == 0 || maLength == 0) {
            return false;
        }
        int i = 0;
        StringBuilder builder = new StringBuilder(magazine);
        while (true) {
            int indexOf = builder.indexOf(String.valueOf(ransomNote.charAt(i)));
            if (indexOf >= 0) {
                builder.deleteCharAt(indexOf);
                i ++;
            } else {
                break;
            }
            if (i == noteLength || builder.length() == 0) {
                break;
            }
        }
        return i == noteLength;
    }

    public static void main(String[] args) {
        MergeKLists mergeKLists = new MergeKLists();
        boolean b = mergeKLists.canConstruct("bjaajgea", "affhiiicabhbdchbidghccijjbfjfhjeddgggbajhidhjchiedhdibgeaecffbbbefiabjdhggihccec");
        System.out.println(b);
    }

}
