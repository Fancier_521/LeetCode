package day190411;

import java.util.Arrays;

/**
 * @author LI DUO
 * @date 2019/4/11 下午 08:29
 */
public class QuickSort {

    public void quickSort(int[] a, int n) {
        quickSort_c(a, 0, n - 1);
    }

    public void quickSort_c(int[] a, int p, int r) {
        if (p >= r) {
            return;
        }
        int q = partition(a, p, r);
        quickSort_c(a, p, q-1);
        quickSort_c(a, q + 1, r);
    }

    public int partition(int[] a, int p, int r) {
        int pivot = a[r];
        int i = p;
        for (int j = p; j <= r - 1; j++) {
            if (a[j] < pivot) {
               int tmp = a[i];
               a[i] = a[j];
               a[j] = tmp;
               i++;
            }
        }
        int tmp = a[i];
        a[i] = a[r];
        a[r] = tmp;
        return i;
    }

    public static void main(String[] args) {
        QuickSort sort = new QuickSort();
        int[] a = new int[]{6, 11, 3, 9, 8};
        sort.quickSort(a, a.length);
        System.out.println(Arrays.toString(a));
    }
}
