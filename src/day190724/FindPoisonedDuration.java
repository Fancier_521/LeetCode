package day190724;

/**
 * @author LI DUO
 * @version 1.0
 * @date 2019/7/24 下午 06:17
 */
public class FindPoisonedDuration {

    public int findPoisonedDuration(int[] timeSeries, int duration) {
        if (timeSeries.length == 0) {
            return 0;
        }
        int result = 0;
        for (int i = 1; i < timeSeries.length; i++) {
            if (timeSeries[i] - timeSeries[i - 1] < duration) {
                result += timeSeries[i] - timeSeries[i - 1];
            } else if (timeSeries[i] - timeSeries[i - 1] > duration) {
                result += duration;
            }
        }
        result += duration;
        return result;
    }
}
