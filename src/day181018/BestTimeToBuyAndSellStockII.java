package day181018;

/**
 * @author LI DUO
 * @date 2018/10/18 下午 03:56
 */
public class BestTimeToBuyAndSellStockII {
    public int maxProfit(int[] prices) {
        int maxProfit = 0;
        for (int i = 1; i < prices.length; i++) {
            if (prices[i] > prices[i - 1]) {
                maxProfit += prices[i] - prices[i - 1];
            }
        }
        return maxProfit;
    }

    public static void main(String[] args) {
        int[] prices = {1, 2, 3, 4, 5};

        BestTimeToBuyAndSellStockII time = new BestTimeToBuyAndSellStockII();
        System.out.println(time.maxProfit(prices));
    }
}
