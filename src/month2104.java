/**
 * @author Li Duo
 * @version 1.0
 * @date 2021/4/28 15:14
 */
public class month2104 {

    public boolean judgeSquareSum(int c) {

        int left = 0;
        int right = (int) Math.sqrt(c);

        while (left < right) {
            int res = (left * left) + (right * right);
            if (res < c) {
                left++;
            } else if (res > c) {
                right--;
            } else {
                return true;
            }
        }
        return false;
    }
}
