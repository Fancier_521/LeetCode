package day190312;

/**
 * @author LI DUO
 * @date 2019/3/12 下午 06:06
 */
public class ListNode {
    public int val;
    public ListNode next;

    public ListNode(int x) {
        val = x;
        next = null;
    }
}
